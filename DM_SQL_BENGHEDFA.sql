--PARTIE 1

--mise en place des tables

CREATE DATABASE IF NOT EXISTS bibliotheque CHARACTER SET "utf8";
USE bibliotheque;

CREATE TABLE IF NOT EXISTS genre (
  code char(5) NOT NULL,
  libelle varchar(80) NOT NULL,
  CONSTRAINT PK_genre PRIMARY KEY (code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS ouvrage (
  isbn int(10) NOT NULL,
  titre varchar(200) NOT NULL,
  auteur varchar(80) NOT NULL,
  genre char(5) NOT NULL,
  edition varchar(80) NOT NULL,
  CONSTRAINT PK_ouvrage PRIMARY KEY (isbn),
  FOREIGN KEY (genre) REFERENCES genre(code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS exemplaire (
  isbn int(10) NOT NULL,
  numero int(3) NOT NULL,
  etat char(5) NOT NULL,
  check(etat in ("NE","BO","MO","MA")),
  PRIMARY KEY (isbn, numero),
  FOREIGN KEY (isbn) REFERENCES ouvrage(isbn)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS membre (
  numero int(10) NOT NULL,
  nom varchar(80) NOT NULL,
  prenom varchar(80) NOT NULL,
  adresse varchar(200) NOT NULL,
  telephone char(10) NOT NULL,
  adhesion date NOT NULL,
  duree int(2) NOT NULL,
  check (duree >= 0),
  CONSTRAINT PK_membre PRIMARY KEY (numero)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS emprunt (
  numero int(10) NOT NULL,
  membre int(6) NOT NULL,
  creeLe TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  CONSTRAINT PK_emprunt PRIMARY KEY (numero),
  FOREIGN KEY (membre) REFERENCES membre(numero)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS detailsemprunt (
  emprunt int(10) NOT NULL,
  numero int(3) NOT NULL,
  isbn int(10) NOT NULL,
  exemplaire int(3) NOT NULL,
  renduLe date NOT NULL,
  duree int(2) NOT NULL,
  check (duree >= 0),
  CONSTRAINT PK_detailsemprunt PRIMARY KEY (emprunt, numero),
  CONSTRAINT FK_detailsemprunt FOREIGN KEY (emprunt) REFERENCES emprunt(numero),
  CONSTRAINT FK_detailsexemplaire FOREIGN KEY (isbn,exemplaire) REFERENCES exemplaire(isbn,numero)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

--création des sequences 

create sequence ma_sequence
	start with 10
	increment by 1
	no cycle;
	
--Ajout de contraintes d’intégrité

alter table membre 
add constraint noduplicate unique(nom, prenom, telephone);

--Modification de table et ajout d’une colonne

alter table membre add telephone_mobile char(10) NOT NULL;

alter table membre add constraint mobile_start check( mobile Like '0[6-7]%' );

--Création d’un index

CREATE INDEX IDX_ouvrage
ON ouvrage (genre);

CREATE INDEX IDX_exemplaire
ON exemplaire (isbn);

CREATE INDEX IDX_emprunt
ON emprunt (membre);

CREATE INDEX IDX_detailsemprunt
ON detailsemprunt (emprunt);

CREATE INDEX IDX_detailsexemplaire
ON detailsexemplaire (isbn,exemplaire);

--Modification d’une contrainte d’intégrité

alter table detailsemprunt drop constraint fk_detailsemprunt;
alter table detailsemprunt add constraint deleteoncascade
foreign key(emprunt)
references emprunt(numero)
on delete cascade;

--Attribution d’une valeur par défaut à une colonne

alter table exemplaire modify etat default 'NE';

--Définition d’un synonyme



--Modification du nom d’une table

alter table detailsemprunt rename to details ;

--PARTIE 2

--Ajout d’informations dans une table

insert into emprunt(Code, libelle) values('REC','Récit');
insert into emprunt(Code, libelle) values('POL','Policier');
insert into emprunt(Code, libelle) values('BD','Bande Dessinée');
insert into emprunt(Code, libelle) values('INF','Informatique');
insert into emprunt(Code, libelle) values('THE','Théâtre');
insert into emprunt(Code, libelle) values('ROM','Roman');


insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2203314168,'LEFRANC-L’ultimatum','Martin','Carin','BD','Casterman');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2746021285,'HTML entrainez-vous pour maitriser le code','Luc Van Lancker','INFO','ENI');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2746026090,'SQL','J. Gabillaud','INF','ENI');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2266085816,'Pantagruel','François Rabelais','ROM','POCKET');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2266091611,'Voyage au centre de la terre','Jules Verne','ROM','POCKET');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2253010219,'Le crime de l’Orient Express','Agatha Christie','POL','Livre Poche');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2070400816,'Le Bourgeois gentilhomme','Molière','THE','Gallimard');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2070367177,'Le curé de Tours','Honoré de Balzac','ROM','Gallimard');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2080720872,'Boule de suif','Guy de Maupassant','REC','Flammarion');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2877065073,'La gloire de mon père','Marcel Pagnol','ROM','Fallois');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2020549522,'L’aventure des manuscrits de la mer morte',,'REC','Seuil');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2253006327,'Vingt mille lieues sous les mers','Jules Verne','ROM','LGF');
insert into ouvrages(ISBN, Titre, Auteur, Genre, Edition) values(2038704015,'De la terre à la lune','Jules Verne','ROM','Larousse');

insert into exemplaire(ISBN, Numero, Etat) values(2203314168,1,'Moyen');
insert into exemplaire(ISBN, Numero, Etat) values(2203314168,2,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2203314168,3,'Neuf');

insert into exemplaire(ISBN, Numero, Etat) values(2746021285,1,'Bon');

insert into exemplaire(ISBN, Numero, Etat) values(2746026090,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2746026090,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2266085816,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2266085816,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2266091611,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2266091611,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2253010219,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2253010219,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2070400816,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2070400816,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2070367177,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2070367177,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2080720872,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2080720872,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2877065073,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2877065073,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2020549522,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2020549522,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2253006327,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2253006327,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2038704015,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2038704015,2,'Moyen');

insert into exemplaire(ISBN, Numero, Etat) values(2203314168,1,'Bon');
insert into exemplaire(ISBN, Numero, Etat) values(2203314168,2,'Moyen');

insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(1,'ALBERT','Anne','13 rue des Alpes','0601020304',Sysdate-60,1);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(2,'BERNAUD','Barnabé','6 rue des bécasses','0602030105',Sysdate-10,3);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(3,'CUVARD','Camille','52 rue des cerisiers','0602010509',Sysdate-100,6);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(4,'DUPOND','Daniel','11 rue des daims','0610236515',Sysdate-250,12);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(5,'EVROUX','Eglantine','34 rue des elfes','0658963125',Sysdate-150,6);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(6,'FREGEON','Fernand','11 rue des Frans','0602036987',Sysdate-400,6);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(7,'GORIT','Gaston','96 rue de la glacerie','0684235781',Sysdate-150,1);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(8,'HEVARD','Hector','12 rue haute','0608546578',Sysdate-250,12);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(9,'INGRAND','Irène','54 rue de iris','0605020409',Sysdate-50,12);
insert into membre(N°,Nom,Prénom,Adresse,Mobile,Adhésion,Durée) values(10,'JUSTE','Julien','5 place des Jacobins','0603069876',Sysdate-100,6);

--Exécution d’un script

insert into emprunt(numero, membre, creele) values(1,1,sysdate-200);
insert into emprunt(numero, membre, creele) values(2,3,sysdate-190);
insert into emprunt(numero, membre, creele) values(3,4,sysdate-180);
insert into emprunt(numero, membre, creele) values(4,1,sysdate-170);
insert into emprunt(numero, membre, creele) values(5,5,sysdate-160);
insert into emprunt(numero, membre, creele) values(6,2,sysdate-150);
insert into emprunt(numero, membre, creele) values(7,4,sysdate-140);
insert into emprunt(numero, membre, creele) values(8,1,sysdate-130);
insert into emprunt(numero, membre, creele) values(9,9,sysdate-120);
insert into emprunt(numero, membre, creele) values(10,6,sysdate-110);
insert into emprunt(numero, membre, creele) values(11,1,sysdate-100);
insert into emprunt(numero, membre, creele) values(12,6,sysdate-90);
insert into emprunt(numero, membre, creele) values(13,2,sysdate-80);
insert into emprunt(numero, membre, creele) values(14,4,sysdate-70);
insert into emprunt(numero, membre, creele) values(15,1,sysdate-60);
insert into emprunt(numero, membre, creele) values(16,3,sysdate-50);
insert into emprunt(numero, membre, creele) values(17,1,sysdate-40);
insert into emprunt(numero, membre, creele) values(18,5,sysdate-30);
insert into emprunt(numero, membre, creele) values(19,4,sysdate-20);
insert into emprunt(numero, membre, creele) values(20,1,sysdate-10);

insert into details(emprunt, numero, isbn, exemplaire, rendule) values(1,1,2038704015,1,sysdate-195);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(1,2,2070367177,2,sysdate-190);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(2,1,2080720872,1,sysdate-180);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(2,2,2203314168,1,sysdate-179);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(3,1,2038704015,1,sysdate-170);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(4,1,2203314168,2,sysdate-155);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(4,2,2080720872,1,sysdate-155);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(4,3,2266085816,1,sysdate-159);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(5,1,2038704015,1,sysdate-140);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(6,1,2266085816,2,sysdate-141);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(6,2,2080720872,2,sysdate-130);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(6,3,2746021285,1,sysdate-133);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(7,1,2070367177,2,sysdate-100);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(8,1,2080720872,1,sysdate-116);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(9,1,2038704015,1,sysdate-100);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(10,1,2080720872,2,sysdate-107);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(10,2,2746026090,1,sysdate-78);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(11,1,2746021285,1,sysdate-81);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(12,1,2203314168,1,sysdate-86);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(12,2,2038704015,1,sysdate-60);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(13,1,2070367177,1,sysdate-65);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(14,1,2266091611,1,sysdate-66);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(15,1,2070400816,1,sysdate-50);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(16,1,2253010219,2,sysdate-41);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(16,2,2070367177,2,sysdate-41);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(17,1,2877065073,2,sysdate-36);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(18,1,2070367177,1,sysdate-14);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(19,1,2746026090,1,sysdate-12);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(20,1,2266091611,1,default);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(20,2,2253010219,1,default);

insert into ouvrage (isbn, titre, auteur, genre, editeur) values (2080703234, 'Cinq semaines en ballon', 'Jules Verne', 'ROM', 'Flammarion');

--Extraction simple d’informations

select * from genre,ouvrage,exemplaire,membre,emprunt,details

